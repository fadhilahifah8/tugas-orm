@extends('layout.master')

@section('judul')
    Halaman Tambah Genre
@endsection

@section('isi')

<div>
    <form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama Genre</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>

@endsection