@extends('layout.master')

@section('judul')
    Halaman Tambah Genre
@endsection

@section('isi')
    <form action="/genre/{{$genre->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Nama Genre</label>
                <input type="text" class="form-control" name="nama" value="genre" value="{{$genre->nama}}" placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection