@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('isi')

<div>
    <form action="/cast/{{$cast->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" name="nama" value="cast" value="{{$cast->nama}}" placeholder="Masukkan Nama Cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <label>Umur</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur Anda"><br>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <label>Bio</label><br>
                <textarea type="text" name="bio" cols="30" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>

@endsection