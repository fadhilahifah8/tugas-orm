<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello-laravel', function () {
    echo "ini halaman baru laravel <br>";
    return "hello laravel";
});

// Route::get('/test', function() {
//     return "OK";
// });

// Route::get('/halo/{nama}', function($nama){
//     return "halo $nama";
// });

// Route::get('/test/{angka}', function($angka) {
//     return view ('test', ["angka" => $angka]);
// });

// Route::get('/form', 'RegisterController@form');
// Route::get('/sapa', 'RegisterController@sapa');
// Route::post('/sapa', 'RegisterController@sapa_post');


// Route::get('/master', function(){
//     return view('layout.master');
// });

// Route::get('/master', function(){
//     return view('adlte.master');
// });

// Route::get('/items', function() {
//     return view('items.index');
// });

// Route::get('items/create', function(){
//     return view('items.create');
// });

// Route::get('/home', 'HomeController@home');
// Route::get('/form', 'FormController@form');
// Route::post('/post', 'FormController@kirim');

// Route::get('/genre', 'genreController@index');
// Route::get('/genre/create', 'genreController@create');
// Route::post('/genre', 'genreController@store');
// Route::get('/genre/{genre_id}', 'genreController@show');
// Route::get('/genre/{genre_id}/edit', 'genreController@edit');
// Route::put('/genre/{genre_id}', 'genreController@update');
// Route::delete('/genre/{genre_id}', 'genreController@destroy');

// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('cast', 'CastController@store');
// Route::get('cast/{cast_id}', 'CastController@show');
// Route::get('cast/{cast_id}/edit', 'CastController@edit');
// Route::put('cast/{cast_id}', 'CastController@update');
// Route::delete('cast/{cast_id}', 'CastController@destroy');

Route::resource('cast', 'castController');

