<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use DB;
use App\cast;

class castController extends Controller
{
    public function index() {
        // $cast = DB::table('cast')->get();
        // return view('cast.index', compact('cast'));
        $cast = cast::all();
        return view('cast.index', compact('cast'));
    }

    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            //'nama' => 'required|unique:cast',
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // $query = DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]

        cast::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);
        return redirect('/cast')->with('succes', 'Berhasil update cast');
    }

    public function show($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        // return view('cast.show', compact('cast'));
        $cast = cast::find($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        // return view('cast.edit', compact('cast'));

        $cast = cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            //'nama' => 'required|unique:cast',
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         'nama' => $request["nama"],
        //         'umur' => $request["umur"],
        //         'bio' => $request["bio"]

        $cast = cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast')->with('succes', 'Berhasil diperbaharui');

        //     ]);
        // return redirect('/cast');
    }

    public function destroy($id) {
        // $query = DB::table('cast')->where('id', $id)->delete();
        // return redirect('/cast');
        $cast = cast::find($id);
        $cast->delete();
        return redirect('/cast')->with('succes', 'Berhasil update cast');
    }
}
